php-horde-date (2.4.1-10) UNRELEASED; urgency=medium

  [ Anton Gladky ]
  * d/salsa-ci.yml: use aptly, simplify.

  [ Mike Gabriel ]
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 19 Jan 2023 08:56:54 +0100

php-horde-date (2.4.1-9) unstable; urgency=medium

  * d/patches: Add 1011_php8.1.patch.
  * d/t/control: Allow output to stderr for now. PHP 8.1 deprecated strftime(),
    this needs upstream work.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 13 Jul 2022 15:52:14 +0200

php-horde-date (2.4.1-8) unstable; urgency=medium

  * d/t/control: Require php-horde-icalendar.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jul 2020 22:36:18 +0200

php-horde-date (2.4.1-7) unstable; urgency=medium

  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).
  * d/t/control: Require locales-all.
  * d/patches: Add debian/patches/1010_phpunit-8.x+9.x.patch.
    Fix tests with PHPUnit 8.x/9.x.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jul 2020 20:28:11 +0000

php-horde-date (2.4.1-6) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:36:41 +0200

php-horde-date (2.4.1-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959254).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:21:40 +0200

php-horde-date (2.4.1-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * salsa-ci.yml: Allow autopkgtest failure
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 19:50:37 +0200

php-horde-date (2.4.1-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 00:20:31 +0200

php-horde-date (2.4.1-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 11:37:32 +0200

php-horde-date (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Sep 2017 21:50:24 +0200

php-horde-date (2.4.0-1) unstable; urgency=medium

  * New upstream version 2.4.0

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Jun 2017 21:26:38 +0200

php-horde-date (2.3.2-1) unstable; urgency=medium

  * New upstream version 2.3.2

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 Dec 2016 21:55:20 +0100

php-horde-date (2.3.1-1) unstable; urgency=medium

  * New upstream version 2.3.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Jul 2016 23:58:04 +0200

php-horde-date (2.3.0-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 22:28:41 +0200

php-horde-date (2.3.0-1) unstable; urgency=medium

  * New upstream version 2.3.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 10:42:22 +0100

php-horde-date (2.2.0-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 22:49:50 +0100

php-horde-date (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 14:12:24 +0100

php-horde-date (2.1.1-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9
  * copyright: No more files under BSD-2-clause

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:23:14 +0200

php-horde-date (2.1.1-1) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:27:51 +0200

php-horde-date (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 16 Jun 2015 21:39:52 +0200

php-horde-date (2.0.13-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.13

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:27:41 +0200

php-horde-date (2.0.12-5) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 12:12:06 +0200

php-horde-date (2.0.12-4) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 10:53:39 +0200

php-horde-date (2.0.12-3) unstable; urgency=medium

  * Fix DEP-8 depends

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:47:36 +0200

php-horde-date (2.0.12-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 24 Aug 2014 09:34:22 +0200

php-horde-date (2.0.12-1) unstable; urgency=medium

  * New upstream version 2.0.12

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 Aug 2014 10:58:29 +0200

php-horde-date (2.0.11-1) unstable; urgency=medium

  * New upstream version 2.0.11

 -- Mathieu Parent <sathieu@debian.org>  Fri, 04 Jul 2014 08:03:44 +0200

php-horde-date (2.0.10-1) unstable; urgency=medium

  * New upstream version 2.0.10

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Jun 2014 20:17:26 +0200

php-horde-date (2.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.9

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Apr 2014 09:14:04 +0200

php-horde-date (2.0.8-1) unstable; urgency=medium

  * New upstream version 2.0.8

 -- Mathieu Parent <sathieu@debian.org>  Sat, 08 Mar 2014 09:16:47 +0100

php-horde-date (2.0.7-1) unstable; urgency=low

  * New upstream version 2.0.7

 -- Mathieu Parent <sathieu@debian.org>  Tue, 22 Oct 2013 17:39:06 +0200

php-horde-date (2.0.6-1) unstable; urgency=low

  * Use pristine-tar
  * New upstream version 2.0.6

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 20:09:16 +0200

php-horde-date (2.0.5-1) unstable; urgency=low

  * New upstream version 2.0.5

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 19:05:49 +0200

php-horde-date (2.0.4-1) unstable; urgency=low

  * New upstream version 2.0.4

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 16:09:09 +0200

php-horde-date (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 20:20:23 +0100

php-horde-date (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:40:48 +0100

php-horde-date (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 21:17:18 +0100

php-horde-date (1.0.9-1) unstable; urgency=low

  * Horde_Date package.
  * Initial packaging (Closes: #635792)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 10 Jan 2012 22:24:12 +0100
